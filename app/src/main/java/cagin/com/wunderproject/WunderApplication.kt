package cagin.com.wunderproject

import android.app.Activity
import android.app.Application
import cagin.com.wunderproject.base.di.components.DaggerAppComponent
import cagin.com.wunderproject.utils.Constants

import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject



class WunderApplication : Application(), HasActivityInjector {

    @Inject lateinit var activityDispatchingAndroidInjector : DispatchingAndroidInjector<Activity>

    override fun activityInjector(): AndroidInjector<Activity> {
        return activityDispatchingAndroidInjector
    }

    override fun onCreate() {
        super.onCreate()
        DaggerAppComponent
                .builder()
                .application(this)
                .endPoint(Constants.Api.BASE_URL)
                .build().inject(this)
    }

}
