package cagin.com.wunderproject.base.di.builder

import cagin.com.wunderproject.feature.carlist.CarListActivity
import cagin.com.wunderproject.feature.carlist.CarListActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [CarListActivityModule::class])
    internal abstract fun contributeCarListActivity(): CarListActivity

}
