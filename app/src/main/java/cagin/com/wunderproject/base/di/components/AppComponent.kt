package cagin.com.wunderproject.base.di.components

import android.app.Application
import cagin.com.wunderproject.base.di.builder.ActivityBuilder
import cagin.com.wunderproject.base.di.module.AppModule
import cagin.com.wunderproject.data.di.RetrofitModule
import cagin.com.wunderproject.WunderApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Named
import javax.inject.Singleton


@Singleton
@Component(modules = [
    RetrofitModule::class,
    AndroidInjectionModule::class,
    AppModule::class,
    ActivityBuilder::class])
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        abstract fun application(application: Application): Builder

        @BindsInstance
        abstract fun endPoint(@Named("apiEndpoint") apiEndpoint: String): Builder

        fun build() : AppComponent
    }
    fun inject(app: WunderApplication)
}
