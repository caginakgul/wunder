package cagin.com.wunderproject.base.di.module

import cagin.com.wunderproject.WunderApplication
import dagger.Module


@Module
class AppModule(val app: WunderApplication) {
//May be used further
}
