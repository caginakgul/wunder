package cagin.com.wunderproject.data.di

import cagin.com.wunderproject.utils.Constants
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.CookieManager
import java.net.CookiePolicy
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
class RetrofitModule {

    @Provides
    @Singleton
    fun provideRetrofit (okHttpClient: OkHttpClient, @Named("apiEndpoint")apiEndpoint: String): Retrofit.Builder {
        return Retrofit.Builder()
                .baseUrl(Constants.Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                //.addCallAdapterFactory(new LiveDataServiceResultCallAdapterFactory())
                .client(okHttpClient)
    }

    @Provides
    @Singleton
    fun provideOkHttpClient (): OkHttpClient {
        val builder = OkHttpClient.Builder()
                .connectTimeout(3, TimeUnit.MINUTES)
                .readTimeout(3, TimeUnit.MINUTES)
                .writeTimeout(3, TimeUnit.MINUTES)
        return builder.build()
    }
}