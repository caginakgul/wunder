package cagin.com.wunderproject.data.repository


import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import cagin.com.wunderproject.data.retrofit.implementation.CarListImplementation
import cagin.com.wunderproject.data.retrofit.model.CarListResponse
import cagin.com.wunderproject.data.retrofit.model.Placemark
import retrofit2.Call
import retrofit2.Response
import javax.inject.Inject

class CarListRepository @Inject constructor(carListImp: CarListImplementation){

    val carListImplementation : CarListImplementation
    init {
        carListImplementation = carListImp
    }

    fun sendCarListRequest(): LiveData<CarListResponse> {
         val data2 :MutableLiveData<CarListResponse> = MutableLiveData()
        carListImplementation.sendCarRequest().enqueue((object : retrofit2.Callback<CarListResponse> {
            override fun onFailure(call: Call<CarListResponse>?, t: Throwable?) {
                Log.d("request","fail")
                data2.value = null
            }
            override fun onResponse(call: Call<CarListResponse>?, response: Response<CarListResponse>?) {
                Log.d("request","success")
                data2.value=response?.body()
            }
        }))
        return data2
    }
}