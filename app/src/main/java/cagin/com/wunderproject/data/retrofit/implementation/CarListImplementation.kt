package cagin.com.wunderproject.data.retrofit.implementation

import cagin.com.wunderproject.data.retrofit.model.CarListResponse
import cagin.com.wunderproject.data.retrofit.model.Placemark
import cagin.com.wunderproject.data.retrofit.services.CarListService
import retrofit2.Call
import retrofit2.Retrofit
import javax.inject.Inject

class CarListImplementation @Inject constructor(builder: Retrofit.Builder) : CarListService {
    private val carListService: CarListService

    init {
        carListService = builder.build().create(CarListService::class.java)
    }

    override fun sendCarRequest(): Call<CarListResponse> {
        return carListService.sendCarRequest()
    }
}