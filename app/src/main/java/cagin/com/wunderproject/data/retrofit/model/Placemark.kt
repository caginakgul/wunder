package cagin.com.wunderproject.data.retrofit.model

import android.os.Parcel
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Placemark(val address: String,val coordinates: DoubleArray,
                     val engineType: String, val exterior: String,
                     val fuel: Int, val interior: String,
                     val name: String, val vin: String) : Parcelable {

}