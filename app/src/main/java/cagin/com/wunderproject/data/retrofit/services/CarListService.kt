package cagin.com.wunderproject.data.retrofit.services

import android.provider.SyncStateContract
import cagin.com.wunderproject.data.retrofit.model.CarListResponse
import cagin.com.wunderproject.data.retrofit.model.Placemark
import cagin.com.wunderproject.utils.Constants
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.POST

interface CarListService {
    @GET("locations.json")
    fun sendCarRequest(): Call<CarListResponse>
}