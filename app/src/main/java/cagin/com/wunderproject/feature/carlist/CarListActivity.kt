package cagin.com.wunderproject.feature.carlist

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import android.widget.Toast
import cagin.com.wunderproject.R
import cagin.com.wunderproject.data.retrofit.model.CarListResponse
import cagin.com.wunderproject.data.retrofit.model.Placemark
import cagin.com.wunderproject.databinding.ActivityCarListBinding
import cagin.com.wunderproject.feature.carmap.MapsActivity
import dagger.android.AndroidInjection
import javax.inject.Inject

class CarListActivity : AppCompatActivity() {
    private lateinit var binding : ActivityCarListBinding
    private lateinit var viewModel : CarListViewModel
    private lateinit var carList : ArrayList<Placemark>
    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        initBinding()
        sendRequest()
        viewModel.sendCarListRequest().observe(this, object : Observer<CarListResponse> {
            override fun onChanged(networkResponse: CarListResponse?) {
                Log.d("data","retrieved.")
                if(networkResponse!=null){
                    setRecycler(networkResponse)
                    carList = ArrayList(networkResponse!!.placemarks)
                    binding.progressBar.visibility=View.GONE
                    binding.imageViewMap.visibility=View.VISIBLE
                }
            }
        })
        binding.imageViewMap.setOnClickListener({ view -> startMap() })
    }

    private fun initBinding(){
        binding = DataBindingUtil.setContentView(this,R.layout.activity_car_list)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(CarListViewModel::class.java)
    }

    private fun sendRequest(){
        viewModel.sendCarListRequest()
    }

    private fun setRecycler(networkResponse: CarListResponse?){
        binding.recylerViewCarList.layoutManager = LinearLayoutManager(this)
        binding.recylerViewCarList.adapter = CarListAdapter(networkResponse?.placemarks)
    }

    private fun startMap(){
        val intent = Intent(this, MapsActivity::class.java)
        intent.putParcelableArrayListExtra("cars",carList)
        startActivity(intent)
    }
}
