package cagin.com.wunderproject.feature.carlist

import android.arch.lifecycle.ViewModelProvider
import cagin.com.wunderproject.base.di.ProjectViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class CarListActivityModule {
    @Provides fun provideCarListViewModel(carListViewModel: CarListViewModel) : ViewModelProvider.Factory{
        return ProjectViewModelFactory(carListViewModel)
    }
}
