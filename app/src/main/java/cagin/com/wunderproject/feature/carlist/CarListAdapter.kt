package cagin.com.wunderproject.feature.carlist
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import cagin.com.wunderproject.data.retrofit.model.Placemark
import cagin.com.wunderproject.databinding.ItemCarBinding


class CarListAdapter(private val items: List<Placemark>?) : RecyclerView.Adapter<CarListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemCarBinding.inflate(inflater)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items!!.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items!![position])

    inner class ViewHolder(val binding: ItemCarBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Placemark) {
            binding.carItem = item
            binding.executePendingBindings()
        }
    }
}
