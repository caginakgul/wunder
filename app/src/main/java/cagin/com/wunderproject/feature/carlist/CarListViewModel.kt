package cagin.com.wunderproject.feature.carlist

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import cagin.com.wunderproject.data.repository.CarListRepository
import cagin.com.wunderproject.data.retrofit.model.CarListResponse
import cagin.com.wunderproject.data.retrofit.model.Placemark
import javax.inject.Inject
class CarListViewModel @Inject constructor( application: Application) : AndroidViewModel(application) {
    private val carResponseObservable: MutableLiveData<CarListResponse>

    @Inject lateinit var carListRepo:CarListRepository

    init {
        carResponseObservable = MutableLiveData()
    }
    fun sendCarListRequest(): LiveData<CarListResponse>{
        return carListRepo.sendCarListRequest()
    }
}