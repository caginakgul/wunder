package cagin.com.wunderproject.feature.carmap

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Parcelable
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import cagin.com.wunderproject.R
import cagin.com.wunderproject.data.retrofit.model.Placemark
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {
    private lateinit var mMap: GoogleMap
    private lateinit var carList: ArrayList<Parcelable>
    private var markerList: ArrayList<Marker> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        carList=this.intent.getParcelableArrayListExtra<Parcelable>("cars")
    }
    // Adding markers according to the coordinates and zoom camera to Hamburg
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        for (car in carList) {
            val carCoordinate = LatLng((car as Placemark).coordinates[1],(car as Placemark).coordinates[0])
            var marker: Marker = mMap.addMarker(MarkerOptions().position(carCoordinate).title(car.name))
            marker.tag="visible"
            markerList.add(marker)
        }
        userLocation()

        val hamburg = LatLng(53.551086, 9.993682)
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(hamburg, 10f))
        //change visibilities of other markers
        mMap.setOnMarkerClickListener(object : GoogleMap.OnMarkerClickListener {
            override fun onMarkerClick(marker: Marker): Boolean {
                if(marker.tag=="visible"){
                    for (markerItem in markerList) {
                        if(markerItem!=marker){
                            markerItem.setVisible(false)
                            marker.tag="gone"
                        }
                    }
                     return false
                }else{ //user taps second time to display all other markers
                    for (markerItem in markerList) {
                        if(markerItem!=marker){
                            markerItem.setVisible(true)
                            marker.tag="visible"
                        }
                    }
                    return true
                }

            }
        })
    }
    private fun userLocation(){
        val permission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            makeRequest()
            Log.i("Permission", "Permission to your location denied")
        }else{
            mMap.setMyLocationEnabled(true)
        }
    }

    private fun makeRequest() {
        ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),101)
    }
    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            101 -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.i("permission", "Permission has been denied by user")
                    Toast.makeText(this, resources.getText(R.string.permission), Toast.LENGTH_SHORT).show()
                } else {
                    Log.i("permission", "Permission has been granted by user")
                    mMap.setMyLocationEnabled(true)
                }
            }
        }
    }
}