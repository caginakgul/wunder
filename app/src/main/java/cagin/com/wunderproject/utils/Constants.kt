package cagin.com.wunderproject.utils

class Constants {
    object Api {
        val BASE_URL = "https://s3-us-west-2.amazonaws.com/wunderbucket/"
        val LOCATIONS = "locations.json"
    }
}